(ns medularis-chalensh.models
  (:use honeysql.helpers
        [environ.core :only [env]])
  (:require [clojure.java.jdbc :as sql]
            [honeysql.core :as hql]
            [cheshire.core :as json]))


(def db-config
  (env :database-url))

(defn save-response
  "Saves the response. The twilio API response is encoded
  as a string

  Returns the whole row including generated id, as the Postgres
  driver does"
  [message api-status api-response]
  (sql/insert! db-config
               :sent_messages
               {:message message
                :api_status api-status
                :api_response (json/encode api-response)}))

(defn get-message
  "Fetches the sms record corresponding to the id.

  The twilio API response is decoded as a JSON hashmap"
  [id]
  (let [result (->> ["SELECT * FROM sent_messages WHERE id = ?" id]
                    (sql/query db-config)
                    first)]
    (and result (update-in result [:api-response] json/decode))))
