(ns medularis-chalensh.core
  (:require [ring.adapter.jetty :as jetty]
            [medularis-chalensh.routes :as routes]
            [taoensso.timbre :as log])
  (:use [environ.core :only (env)]))

(defn -main [& _]
  (log/info "Environment variables: " (select-keys env [:twilio-sid :auth-token :database-url :twilio-from :twilio-to]))
  (let [port (Integer. (or (env :port) 5000))]
    (jetty/run-jetty routes/api {:port port :join? false})))
