(ns medularis-chalensh.routes-test
  (:use clojure.test
        medularis-chalensh.routes
        ring.util.response)
  (:require [clj-http.client :as http]
            [medularis-chalensh.models :as model]
            [ring.mock.request :as mock]
            [clj-time.core :as t]
            [clj-time.coerce :as tc]))

(deftest send-message-test
  (with-redefs [post-twilio (fn [message]
                              (-> (response {:code 20003
                                             :message "Authenticate"})
                                  (status 401)))]
    (is (= (sms (mock/request :post "/api/sms" {:body {:message "Dummy"}}))
           (-> (response {:message "Internal server error"})
               (status 500)))
        "Auth error leaks"))
  (with-redefs [post-twilio (fn [message]
                              (-> (response {:message "Message body is required."})
                                  (status 400)))]
    (is (= (sms (mock/request :post "/api/sms" {:body {}}))
           (-> (response {:message "Message body is required."})
               (status 400))))))

(deftest get-message-test
  (with-redefs [model/get-message (fn [id]
                                    {:id 1
                                     :status "queued"
                                     :api-status 201
                                     :api-response {:dummy 1}
                                     :created-at (tc/to-sql-date (t/date-time 2016 6 10 15 56))})]
    (is (= (sms (mock/request :get "/api/sms/1"))
                                    (response {:id 1
                                               :status "queued"
                                               :api-status 201
                                               :api-response {:dummy 1}
                                               :created-at (tc/to-sql-date (t/date-time 2016 6 10 15 56))}))))
  (with-redefs [model/get-message (fn [id]
                                    nil)]
    (is (= (sms (mock/request :get "/api/sms/1"))
           (-> (response {:message "Not found"})
               (status 404)))))

  (is (= (:status (sms (mock/request :get "/api/sms/dummy")))
         422)))

(deftest exception-handling-test
  (with-redefs [send-message (fn [_]
                               (throw (Exception. "Some type of exception")))]
    (is (= (api (mock/request :post "/api/sms" {:body {:message "Dummy"}}))
           {:status 500
            :headers {"Content-Type" "application/json; charset=utf-8"}
            :body "{\"message\":\"Internal server error\"}"}))))
