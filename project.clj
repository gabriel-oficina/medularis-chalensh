(defproject medularis-chalensh "0.1.0-SNAPSHOT"
  :description "Solution for Medularis's IRON PROGRAMMER CHALLENGE"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [;; For processing requests
                 [org.clojure/clojure "1.7.0"]
                 [compojure "1.5.0"]
                 [ring "1.4.0"]
                 [ring/ring-jetty-adapter "1.4.0"]
                 [ring/ring-mock "0.3.0"]

                 ;; For making requests
                 [clj-http "2.2.0"]

                 ;; JSON
                 [cheshire "5.6.1"]
                 [ring/ring-json "0.4.0"]

                 ;; DB
                 [org.clojure/java.jdbc "0.6.1"]
                 [org.postgresql/postgresql "9.4-1201-jdbc41"]
                 [honeysql "0.6.3"]

                 ;; Environment variables for configs
                 [environ "0.5.0"]

                 ;; Testing
                 [clj-time "0.12.0"]

                 ;; Logging
                 [com.taoensso/timbre "4.3.1"]]
  :min-lein-version "2.0.0"
  :uberjar-name "medularis-chalensh-standalone.jar"
  :plugins [[quickie "0.4.1"]]
  :main medularis-chalensh.core)
